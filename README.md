# MDCNette | [Dialog](https://github.com/material-components/material-components-web/tree/master/packages/mdc-dialog)
Material components DIALOG implementation to NETTE Framework.

*Idea behind was inspired by [Ipublikuj](https://github.com/iPublikuj/confirmation-dialog).*
## Install

The best way to get MDCNette dialog is via composer.
```
php composer.phar require mdcnette/dialog
```
In config add MDCNette dialog extension.
```neon
extensions:
    mdcdialog: MDCNette\Dialog\DI\DialogExtension
```
### Client side (js/css)
And finally you need to setup javascript...
The best approach is to install it with npm.
```npm
npm install @mdcnette/dialog
```
*or you can find it in this repo in `client-side` folder.*

in `dist` folder you can find files to include.

- `material-components-web.min.css` 

*(styles for dialog and all of other material components)* link this to your head in html layout.
- `material-components-web.min.js` 

*(base js for dialog and all of other material components)* add this to your script load.

- `mdcnette.ajax.dialog.js` /or/ `mdcnette.noajax.dialog.js`

*(js for dialog)* add this to you script load.

@layout.latte preview
```html
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="path/to/material-components-web.min.css" type="text/css">
    
    <!--Optionaly you will want google fonts and icons-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Mono">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
{include content}

<script src="path/to/material-components-web.min.js" type="text/javascript"></script>

<!--AJAX-->
<script src="path/to/nette.ajax.js" type="text/javascript"></script>
<script src="path/to/mdcnette.ajax.dialog.js" type="text/javascript"></script>
// Do not forget to initialize nette.ajax...
<!--or NO AJAX-->
<script src="path/to/mdcnette.noajax.dialog.js" type="text/javascript"></script>
</body>

</html>
```

*After this you can start using material snackbar!*

## Usage

In presenter add Trait (normally in BasePresenter)
```php
use MDCNette\Dialog\TDialog;
```
Wherever you want to have dialog, create component for it.
```php
public function createComponentPermissionDialog() {
    $dialog = $this->dialogFactory->create();

    $dialog->addDialog(
        'add',                                                              // Name
        'Are you sure?',                                                    // Title    
        'You cannot take this action back'                                  // Optional Message
        ['add permission' => [$this, 'addPermission'], 'dismiss' => null]   // Actions ['buttonText' => [callable] || null]
    );

    return $dialog;
}

public function addPermission(MDCNette\Dialog\Components\Dialog, int $id){
    // doMagic
}
```
In template.
```latte
{control permissionDialog}
<a n:href="permissionDialog:add! id => $userId">Snackbar</a>
```

And you are good to go!

### How to chain dialogs
Dialogs can be chained.

```php
use MDCNette\Dialog\TDialog;

public function createComponentPermissionDialog() {
    $dialog = $this->dialogFactory->create();

    $dialog->addDialog(
        'add',
        'Giving important permission',
        [$this, 'getMessage'], // you can also add calback as message (title also)
        ['add permission' => [$this, 'addPermission'], 'nope' => null]
    );

    $dialog->addDialog(
        'addForce',
        'Are you sure?',
        'This user is badass! You cannot take this action back!'
        ['force add' => [$this, 'addForcePermission'], 'i made my mind' => null]
    );

    return $dialog;
}

public function getMessage(MDCNette\Dialog\Components\Dialog $dialog, int $id) {
    $user = //GET USER BY $id 
    return 'Do you want to give admin permission to '. $user['name'] . ' ' . $user['surname'];  
}

public function addPermission(int $id) {
    if (/*check User*/) {
        $this['permissionDialog']->showDialog('addForce', ['id' => $id]);
    }
}

public function addForcePermission(int $id) {
    //DO MAGIC HERE
}
```

## Options

There are some options which you can set up in config
```neon
extensions:
    mdcdialog: MDCNette\Dialog\DI\DialogExtension
    
mdcdialog:
    ajax: true       # default value (boolean)
    scrolling: false # default value (boolean)
    icon: false      # default value (string - material icon name)
    button: dismiss  # default value (string)
```

* `ajax` makes form submitting over ajax
* `scrolling` makes content scrollable 
  * *scrolling is made for long text inside dialog. For more information take a look for material component `dialog`*
* `icon` adds material icons in front of title 
* if `button` is not specify there will be one button with no actions = just to close dialog

Or you can set up inside component.
```php
public function createComponentPermissionDialog() {
    $dialog = $this->dialogFactory->create();

    $dialog->setAjax(true);
    $dialog->setScrolling(false);
    $dialog->setIcon('stars');

    return $dialog;
}
```
Or for individual dialog.
```php
public function createComponentPermissionDialog() {
    $dialog = $this->dialogFactory->create();

    $dialog->addDialog('add',[$this, 'addPermission'],'Are you sure?');
    
    $dialog->getDialog('add')->setAjax(true);
    $dialog->getDialog('add')->setScrolling(false);
    $dialog->getDialog('add')->setIcon('stars');

    return $dialog;
}
```
