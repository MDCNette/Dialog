<?php
/**
 * MDCNette Dialog
 * @copyright      More in license.md
 * @license        MIT
 * @author         Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Dialog\Storage;


use Nette\Http\SessionSection;
use Nette\InvalidArgumentException;

/**
 * Class Session
 * @package MDCNette\Dialog\Storage
 * @author  Tomáš Němec <nemi@skaut.cz>
 */
class Session {

    /** @var  SessionSection */
    private $section;

    /**
     * Storage constructor.
     *
     * @param \Nette\Http\Session $session
     *
     * @throws InvalidArgumentException
     */
    public function __construct(\Nette\Http\Session $session) {
        $this->section = $session->getSection('mdcnette.dialog');
    }

    /**
     * @param SessionSection $section
     */
    public function setSection(SessionSection $section) {
        $this->section = $section;
    }

    /**
     * @return SessionSection
     */
    public function getSection(): SessionSection {
        return $this->section;
    }
}