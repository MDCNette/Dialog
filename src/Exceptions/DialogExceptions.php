<?php
/**
 * MDCNette Dialog
 * @copyright      More in license.md
 * @license        MIT
 * @author         Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Dialog\Exceptions;


use Exception;

/**
 * Base class for all dialog exceptions.
 * @package MDCNette\Dialog\Exceptions
 */
class DialogExceptions extends Exception {

}

/**
 * The exception that is thrown when an argument does not match with the expected value.
 */
class InvalidArgumentException extends DialogExceptions {

}