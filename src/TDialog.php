<?php
/**
 * MDCNette Dialog
 * @copyright      More in license.md
 * @license        MIT
 * @author         Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Dialog;

use MDCNette\Dialog\Components\IControl;


/**
 * Trait TMDCNetteD
 * @package TMdcnetteDialog
 * @author  Tomáš Němec <nemi@skaut.cz>
 */
trait TDialog {

    /** @var  IControl */
    protected $dialogFactory;

    /**
     * @param IControl $dialogFactory
     */
    public function injectMdcDialog(IControl $dialogFactory) {
        $this->dialogFactory = $dialogFactory;
    }
}
