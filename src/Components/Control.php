<?php
/**
 * MDCNette Dialog
 * @copyright      More in license.md
 * @license        MIT
 * @author         Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Dialog\Components;


use MDCNette\Dialog\Exceptions\DialogExceptions;
use Nette\Application\UI\Multiplier;
use Nette\DI\Config\Helpers;
use Nette\InvalidArgumentException;
use Nette\Utils\Arrays;

/**
 * Class Control
 * @package MDCNette\Dialog\Components
 * @author  Tomáš Němec <nemi@skaut.cz>
 */
class Control extends \Nette\Application\UI\Control {

	/** @var  IDialog */
	private $dialogFactory;

	/** @var  Dialog */
	private $dialog;

	/** @var  bool */
	private $ajax;

	/** @var  bool */
	private $scrolling;

	/** @var  string */
	private $icon;

	/** @var  array */
	private $defaults;

	/**
	 * Control constructor.
	 *
	 * @param IDialog $dialogFactory
	 */
	public function __construct(IDialog $dialogFactory) {
		$this->dialogFactory = $dialogFactory;
	}

	/**
	 * @param $signal
	 *
	 * @return string
	 */
	public static function formatSignalMethod($signal): string {
		return 'handleShowDialog';
	}

	/**
	 * @param string               $name
	 * @param callable|string      $title
	 * @param callable|string|null $content
	 * @param array|string|null    $actions
	 *
	 * @throws \Nette\InvalidArgumentException
	 * @throws \MDCNette\Dialog\Exceptions\InvalidArgumentException
	 */
	public function addDialog(string $name, $title, $content = NULL, $actions = NULL) {
		$options = array_filter(Arrays::mergeTree(['ajax' => $this->ajax, 'scrolling' => $this->scrolling, 'icon' => $this->icon], $this->defaults));

		$dialog = $this->getDialog($name);
		$dialog->setIcon(Arrays::get($options, 'icon', null));
		$dialog->setAjax(Arrays::get($options, 'ajax', false));
		$dialog->setScrolling(Arrays::get($options, 'scrolling', false));
		$dialog->setTitle($title);
		$dialog->setContent($content);
		$dialog->setActions($actions ?? Arrays::get($options, 'button', 'dismiss'));
	}

	/**
	 * @param string               $name
	 * @param callable|string|null $content
	 * @param array|string|null    $actions
	 *
	 * @throws \Nette\InvalidArgumentException
	 * @throws \MDCNette\Dialog\Exceptions\InvalidArgumentException
	 */
	public function addAlertDialog(string $name, $content = NULL, $actions = NULL) {
		$options = array_filter(Arrays::mergeTree(['ajax' => $this->ajax, 'scrolling' => $this->scrolling, 'icon' => $this->icon], $this->defaults));

		$dialog = $this->getDialog($name);
		$dialog->setIcon(Arrays::get($options, 'icon', null));
		$dialog->setAjax(Arrays::get($options, 'ajax', false));
		$dialog->setScrolling(Arrays::get($options, 'scrolling', false));
		$dialog->setContent($content);
		$dialog->setActions($actions ?? Arrays::get($options, 'button', 'dismiss'));
	}

	/**
	 * @param       $name
	 * @param array $params
	 *
	 * @throws \Nette\InvalidStateException
	 * @throws DialogExceptions
	 */
	public function showDialog($name, array $params = []) {
		$this->dialog = $this[ 'dialog-'.$name ];
		if (!$this->dialog->isConfigured()) {
			throw new DialogExceptions('Dialog with name \''.$name.'\' doesn\'t exist.');
		}
		$this->dialog->showDialog($params);
	}

	/**
	 * @internal
	 * @throws \Nette\InvalidStateException
	 * @throws DialogExceptions
	 */
	public function handleShowDialog() {
		[, $name] = $this->getPresenter()->getSignal();

		$this->showDialog($name, $this->getParameters());
	}

	/**
	 * @return Multiplier
	 */
	public function createComponentDialog(): Multiplier {
		return new Multiplier(function () {
			return $this->dialogFactory->create();
		});
	}

	/**
	 *
	 */
	public function render() {
		$this->template->setFile(__DIR__.'/template/layout.latte');

		$this->template->dialog = $this->dialog;

		$this->template->render();
	}

	/************ CUSTOMIZATION **********
	 *
	 * @param bool $bool
	 */

	public function setAjax(bool $bool) {
		$this->ajax = $bool;
	}

	/**
	 * @param bool $bool
	 */
	public function setScrolling(bool $bool) {
		$this->scrolling = $bool;
	}

	/**
	 * @param string $icon
	 */
	public function setIcon(string $icon) {
		$this->icon = $icon;
	}

	/**
	 *
	 */
	public function resetDialog() {
		$this->dialog = NULL;
		$this->redrawControl();
	}

	/**
	 * @param string $name
	 *
	 * @return Dialog
	 * @throws \Nette\InvalidArgumentException
	 */
	public function getDialog(string $name): Dialog {
		$dialog = $this->getComponent('dialog-'.$name);

		if (!$dialog instanceof Dialog) {
			throw new InvalidArgumentException(sprintf('Dialog control "%s" does not exists', $name));
		}

		return $dialog;
	}

	/**
	 * @param array $defaults
	 */
	public function setDefaults(array $defaults) {
		$this->defaults = $defaults;
	}
}