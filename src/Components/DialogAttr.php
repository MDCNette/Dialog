<?php
/**
 * MDCNette Dialog
 * @copyright      More in license.md
 * @license        MIT
 * @author         Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Dialog\Components;

use MDCNette\Dialog\Exceptions\DialogExceptions;
use MDCNette\Dialog\Exceptions\InvalidArgumentException;
use Nette\Application\UI\Control;
use Nette\Http\Session;


/**
 * Class DialogAttr
 * @package MDCNette\Dialog\Components
 * @author  Tomáš Němec <nemi@skaut.cz>
 */
abstract class DialogAttr extends Control {

	/** @var  callable */
	protected $handler;

	/** @var  callable|string */
	protected $title;

	/** @var  string */
	protected $content;

	/** @var  bool */
	protected $ajax;

	/** @var  bool */
	protected $scrolling;

	/** @var  string */
	protected $icon;

	/** @var string */
	protected $buttonFocus;

	/** @var  array */
	protected $actions;

	/** @var  Session */
	protected $session;

	/**
	 * DialogAttr constructor.
	 *
	 * @param \MDCNette\Dialog\Storage\Session $session
	 */
	public function __construct(\MDCNette\Dialog\Storage\Session $session) {
		$this->session = $session->getSection();
	}

	/**
	 * @return string
	 * @throws \MDCNette\Dialog\Exceptions\DialogExceptions
	 */
	public function getContent(): string {
		if (\is_callable($this->content)) {
			$content = $this->getCallable('content');

			return (string)$content;
		}

		return (string)$this->content;
	}

	/**
	 * @param string|array|null $content
	 */
	public function setContent($content) {
		if ($this->isCallableOrString($content)) {
			$this->content = $content;
		}
	}

	/**
	 * @param callable|string $title
	 */
	public function setTitle($title) {
		if ($this->isCallableOrString($title)) {
			$this->title = $title;
		}
	}

	/**
	 * @return callable|string
	 * @throws \MDCNette\Dialog\Exceptions\DialogExceptions
	 */
	public function getTitle() {
		if (\is_callable($this->title)) {
			$title = $this->getCallable('title');

			return (string)$title;
		}

		return (string)$this->title;
	}

	/**
	 * @param string $btnText
	 *
	 * @return callable
	 */
	public function getHandler(string $btnText): callable {
		return $this->actions[ $btnText ];
	}

	/**
	 * @param string $btnText
	 * @param array  $params
	 *
	 * @return mixed
	 */
	public function callHandler(string $btnText, array $params) {
		$handler = $this->getHandler($btnText);

		return \call_user_func_array($handler, $params);
	}

	/************ OPTIONS **********
	 *
	 * @param bool $bool
	 */

	public function setAjax(bool $bool) {
		$this->ajax = $bool;
	}

	/**
	 * @param bool $bool
	 */
	public function setScrolling(bool $bool) {
		$this->scrolling = $bool;
	}

	/**
	 * @param null|string $icon
	 */
	public function setIcon(?string $icon) {
		$this->icon = $icon;
	}

	/**
	 * @return string
	 */
	public function getButtonFocus(): string {
		if ($this->buttonFocus) {
			return $this->buttonFocus;
		}

		return key($this->actions);
	}

	/**
	 * @param string $buttonFocus
	 */
	public function setButtonFocus(string $buttonFocus): void {
		$this->buttonFocus = $buttonFocus;
	}

	/**
	 * @param array|string $actions
	 *
	 * @throws InvalidArgumentException
	 */
	public function setActions($actions) {
		if (\is_array($actions)) {
			foreach ($actions as $button => $action) {
				if ($action === NULL || \is_callable($action)) {
					$this->actions[ $button ] = $action;
				} else {
					throw new InvalidArgumentException('Expected callable or null for button \''.$button.'\', got '.\gettype($action));
				}
			}
		} elseif (\is_string($actions)) {
			$this->actions[ $actions ] = NULL;
		} else {
			throw new InvalidArgumentException('Expected array or string, got'.\gettype($actions));
		}
	}

	/************ HELPERS **********
	 *
	 * @param string $attribute
	 *
	 * @return mixed
	 * @throws InvalidArgumentException
	 * @throws DialogExceptions
	 */
	public function getCallable(string $attribute) {
		$token = $this['form']['token']->value;

		if ($token === NULL) {
			throw new InvalidArgumentException('Token is not set!');
		}

		if ($this->session->{$token} === null) {
			throw new DialogExceptions('Dialog is not configured');
		}

		return \call_user_func($this->{$attribute}, $this, $this->session->{$token});
	}

	/**
	 * @param $value
	 *
	 * @return bool
	 */
	private function isCallableOrString($value): bool {
		return (\is_string($value) || \is_callable($value));
	}
}