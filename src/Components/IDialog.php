<?php
/**
 * MDCNette Dialog
 * @copyright      More in license.md
 * @license        MIT
 * @author         Tomáš Němec <nemi@skaut.cz>
 */

namespace MDCNette\Dialog\Components;


/**
 * Interface IDialog
 * @package MDCNette\Dialog\Components
 * @author  Tomáš Němec <nemi@skaut.cz>
 */
interface IDialog {


    /**
     * @return Dialog
     */
    public function create(): Dialog;

}