<?php
/**
 * MDCNette Dialog
 * @copyright      More in license.md
 * @license        MIT
 * @author         Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Dialog\Components;


use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\Button;
use Nette\InvalidStateException;
use Nette\Utils\Strings;

/**
 * Class Dialog
 * @package MDCNette\Dialog\Components
 * @author  Tomáš Němec <nemi@skaut.cz>
 */
class Dialog extends DialogAttr {

	/** @var IContainer */
	private $dialog;

	/**
	 * @return Form
	 * @throws \Nette\InvalidStateException
	 */
	public function createComponentForm(): Form {
		$form = new Form();

		$form->addHidden('token');

		foreach ($this->actions as $btnText => $action) {
			$form->addSubmit(Strings::webalize(str_replace(' ', '', $btnText)), $btnText)
				->onClick[] = function (Button $button) use ($action, $btnText) {


				$this->getDialog()->resetDialog();
				{
					$token = $button->getForm(TRUE)->getValues()->token;
					if ($action && $this->session->{$token}) {
						$this->callHandler($btnText, $this->session->{$token});
					}
					unset($this->session->{$token});
				}
			};
		}

		return $form;
	}

	/**
	 * @param array $params
	 *
	 * @throws \Nette\InvalidStateException
	 */
	public function showDialog(array $params = []) {
		$token = $this->generateToken();

		$this['form']['token']->value = $token;

		$this->session->{$token} = $params;

		$this->redrawControl();
		$this->getDialog()->redrawControl();
	}

	/**
	 * @return Control
	 * @throws \Nette\InvalidStateException
	 */
	public function getDialog(): Control {
		if (!$this->dialog) {
			$multiplier = $this->getParent();
			if ($multiplier instanceof Multiplier) {
				$this->dialog = $multiplier->getParent();

				if (!$this->dialog instanceof Control) {
					throw new InvalidStateException('Dialog is not attached to parent control');
				}
			}
		}

		return $this->dialog;
	}

	/**
	 * Render Dialog
	 *
	 * @throws \MDCNette\Dialog\Exceptions\DialogExceptions
	 */
	public function render() {
		$this->template->setFile(__DIR__.'/template/dialog.latte');
		$this->template->title = $this->getTitle();
		$this->template->content = $this->getContent();
		$this->template->scrolling = $this->scrolling;
		$this->template->ajax = $this->ajax;
		$this->template->icon = $this->icon;
		$this->template->buttonFocus = $this->getButtonFocus();
		$this->template->render();
	}

	/**
	 * Check if confirmer is configured
	 *
	 * @return bool
	 */
	public function isConfigured(): bool {
		return ((\is_string($this->title) || \is_callable($this->title) )
			|| (\is_string($this->content) || \is_callable($this->content)));
	}

	/**
	 * Generate unique token key
	 * @return string
	 */
	private function generateToken(): string {
		return base_convert(md5(uniqid('dialog'.$this->getName(), TRUE)), 16, 36);
	}

}