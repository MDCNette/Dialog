<?php
/**
 * MDCNette Dialog
 * @copyright      More in license.md
 * @license        MIT
 * @author         Tomáš Němec <nemi@skaut.cz>
 */

namespace MDCNette\Dialog\Components;

/**
 * Interface IControl
 * @package MDCNette\Dialog\Components
 * @author  Tomáš Němec <nemi@skaut.cz>
 */
interface IControl {


    /**
     * @return Control
     */
    public function create(): Control;

}