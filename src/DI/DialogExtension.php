<?php
/**
 * MDCNette Dialog
 * @copyright      More in license.md
 * @license        MIT
 * @author         Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Dialog\DI;


use MDCNette\Dialog\Components\Control;
use MDCNette\Dialog\Components\Dialog;
use MDCNette\Dialog\Components\IControl;
use MDCNette\Dialog\Components\IDialog;
use MDCNette\Dialog\Storage\Session;
use Nette\DI\CompilerExtension;

class DialogExtension extends CompilerExtension {

	public function loadConfiguration(): void {
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('session'))
			->setFactory(Session::class);

		$builder->addFactoryDefinition($this->prefix('dialog'))
			->setImplement(IDialog::class)
			->getResultDefinition()->setFactory(Dialog::class);

		$builder->addFactoryDefinition($this->prefix('control'))
			->setImplement(IControl::class)
			->getResultDefinition()->setFactory(Control::class)
			->addSetup('setDefaults', [$this->config]);
	}
}