$(document).ready(function () {
    'use strict';

    const MDCDialog = mdc.dialog.MDCDialog;

    $.nette.ext('dialogs', {
        init: function () {
            let dialogEl;
            if ((dialogEl = document.querySelector('#mdcnette-dialog')) !== null) {
                let dialog;
                if (dialog = new MDCDialog(dialogEl)) {
                    $('#mdcnette-dialog').find('.mdc-dialog__footer__button').click(function () {
                        dialog.close();
                    });
                    dialog.open();
                    $(dialogEl).find('button.focus').focus();
                }
            }
        },
        load: function () {
            this.ext('snippets', true).after(function ($el) {
                let dialogEl;
                if ((dialogEl = $el[0].querySelector('#mdcnette-dialog')) !== null) {
                    setTimeout(function () {
                        let dialog = new MDCDialog(dialogEl);
                        $($el[0].querySelector('#mdcnette-dialog')).find('.mdc-dialog__footer__button').click(function () {
                            dialog.close();
                        });
                        dialog.open();
                        $(dialogEl).find('button.focus').focus();
                    }, 75)
                }
            });
        }
    }, {});

});

